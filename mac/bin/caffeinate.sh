#!/bin/bash

get_caffeine_pid() {
    ps aux | awk '/[c]affeinate -d/ { print $2 }' | tail -1
}

CAFFPID=$(get_caffeine_pid)

update_sketchybar() {
    if ps aux | grep -q 'bin/[s]ketchybar'; then
        "$(brew --prefix)/bin/sketchybar" --trigger caffeinate
    fi
}

return_status() {
    if [[ -n "${1}" ]]; then
        CAFFSTATUS="enabled"
    else
        CAFFSTATUS="disabled"
    fi

    echo "${CAFFSTATUS}"
}

if [[ -n "$CAFFPID" ]]; then
    kill "$CAFFPID"
else
    caffeinate -d &
fi

return_status "$(get_caffeine_pid)"

update_sketchybar
exit
