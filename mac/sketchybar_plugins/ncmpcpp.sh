#!/bin/bash

NCMPCPP="/opt/homebrew/bin/ncmpcpp"

FORMAT="%a - %t"

CURRENTSONG="$("${NCMPCPP}" --current-song="${FORMAT}")"

if [[ -n "${CURRENTSONG}" ]]; then
    sketchybar -m --set $NAME update_freq=15 label="🎵 ${CURRENTSONG}"
else
    sketchybar -m --set $NAME update_freq=15 label=""
fi
