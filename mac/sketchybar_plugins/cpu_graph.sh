#!/usr/bin/env bash

CORE_COUNT=$(sysctl -n machdep.cpu.thread_count)
CPU_INFO=$(ps -eo pcpu)
CPU_SYS=$(echo "$CPU_INFO" | awk "{sum+=\$1} END {print sum/(100.0 * $CORE_COUNT)}")

echo "cpu_sys: $CPU_SYS"

sketchybar -m --push cpu_sys $CPU_SYS


