#!/bin/bash

SPACES="$(yabai -m query --spaces | jq -r '.[] | select(.display==1) | [.index, ."is-visible"] | @sh')"

echo "$SPACES"
echo "$SENDER"

if [[ "$SENDER" != "space_change" ]]; then
    args=()
    while read -r index visible; do
        if [[ "$visible" == "true" ]]; then
            args+=(--add space "space${index}" left --set "space${index}" associated_display=1 "associated_space=${index}" "icon=${index}")
        else
            args+=(--add space "space${index}" left --set "space${index}" associated_display=1 "associated_space=${index}" "icon=${index}")
        fi
    done <<< "${SPACES}"

    sketchybar -m "${args[@]}"
fi

