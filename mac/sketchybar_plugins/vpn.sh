#!/bin/bash

VPNIP=$(ifconfig | awk '/utun.*UP,/,/inet/ { if ($0 ~ "inet ") { print $2 }}')

[[ -z "$VPNIP" ]] && VPNIP="Off"

VPNSTATUS="VPN: $VPNIP"
echo "${VPNSTATUS}"

DNSINFO="$(scutil --dns | sed -n -e '/scoped queries/,/reach/d' -e '/resolver \#1/,/^$/p')"

DNSLIST="$(awk '/nameserver/ { print $1" "$3 }' <<< "$DNSINFO")"
echo "$DNSLIST"

sketchybar -m --set $NAME update_freq=30 label="${VPNSTATUS}" \
                    click_script="sketchybar -m --set \$NAME popup.drawing=toggle" \
                    popup.background.color=0xFF000000
COUNT=0
while read -r nameserver ip; do
    sketchybar -m --add item dns.$COUNT popup.$NAME \
                  --set dns.$COUNT label="$nameserver: $ip"
    echo "$nameserver: $ip"
    COUNT=$(( COUNT + 1 ))
done <<< "${DNSLIST}"
