#!/bin/bash

CAFFEINE_STATUS=$(ps aux | awk '/[c]affeinate -d/ { print $2 }' | tail -1)
echo "caffeine PID: $CAFFEINE_STATUS"
CAFFEINE_STATUS=$([[ -n "$CAFFEINE_STATUS" ]] && echo "☕" || echo "💤")
echo "${CAFFEINE_STATUS}"
sketchybar -m --set $NAME label="${CAFFEINE_STATUS}" \
                          y_offset=2
