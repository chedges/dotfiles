#!/usr/bin/env bash

RED="0xFFFF0000"
GREEN="0xFF00FF00"
YELLOW="0xFFFFFF00"

CURRENTCAPACITY="$(pmset -g batt | grep -Eo '[0-9]{2,3}\%' | tr -d '%')"

if (( CURRENTCAPACITY > 49 )); then
    STATUSCOLOR="${GREEN}"
elif (( CURRENTCAPACITY > 19 )); then
    STATUSCOLOR="${YELLOW}"
else
    STATUSCOLOR="${RED}"
fi

echo "${STATUSCOLOR}"

sketchybar -m --set $NAME label="${CURRENTCAPACITY}" \
                          label.color="${STATUSCOLOR}"
