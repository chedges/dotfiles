#!/bin/bash

CONFIG="${HOME}/.config/jira/config.yml"

if [[ ! -f "${CONFIG}" ]]; then
    echo "Error: missing config, expected at ${HOME}/.config/jira/config.yml"
    exit 1
fi

NAMELOWER="$(tr '[:upper:]' '[:lower:]' <<< "${NAME}")"

ASSIGNEE=$(yq '.assignee' "${CONFIG}")

YQQUERY=".servers.${NAMELOWER}"

PAT="$(yq "${YQQUERY}.personal_access_token" "${CONFIG}")"
URL="$(yq "${YQQUERY}.url" "${CONFIG}")"
JQL="assignee=${ASSIGNEE}%20AND%20$(yq "${YQQUERY}.jql" "${CONFIG}")"

TICKETS="$(curl -s -H "Authorization: Bearer ${PAT}" "${URL}/rest/api/latest/search?jql=${JQL}")"
TOTAL="$(jq '.total' <<< "${TICKETS}")"
TICKETLIST="$(jq -r '.issues[] | [.key, .fields.summary] | @sh' <<< "${TICKETS}")"
TICKETLIST="$(sed '1!G;h;$!d' <<< "$TICKETLIST")"
sketchybar -m --set $NAME update_freq=60 label="${NAME}: ${TOTAL}" \
                    click_script="sketchybar -m --set \$NAME popup.drawing=toggle" \
                    popup.background.color=0xFF000000

echo "Tickets:"
COUNT=0
while read -r id summary; do
    id="$(sed -e 's/^.//g' -e 's/.$//g' <<< "$id")"
    summary="$(sed -e 's/^.//g' -e 's/.$//g' <<< "$summary")"
    echo "${id}: ${summary}"
    sketchybar -m --add item $NAME.ticket.$COUNT popup.$NAME \
                  --set $NAME.ticket.$COUNT label="$id: $summary" \
                                      click_script="open ${URL}/browse/$id"
    COUNT=$(( COUNT + 1 ))
done <<< "${TICKETLIST}"
echo "Total Open Tickets: $TOTAL"
