#!/usr/bin/env bash

### clock.conf lines: Location Timezone, e.g. Beijing Asia/Shanghai

CONFIGFILE="${HOME}/.config/sketchybar/plugins/clock.conf"

sketchybar -m --set $NAME label="$(date "+%A %B %d %R")" \
              click_script="sketchybar -m --set \$NAME popup.drawing=toggle" \
              popup.background.color=0xFF000000

if [[ -f "${CONFIGFILE}" ]]; then
    while read -r TZNAME TZID; do
        echo "${TZNAME}"
        echo "${TZID}"
        TZDATE="$(TZ="${TZID}" date "+%a %b %d %R")"
        sketchybar -m --add item $NAME.tz.$TZNAME popup.$NAME \
                      --set $NAME.tz.$TZNAME label="$TZNAME $TZDATE"
    done < "${CONFIGFILE}"
fi
