#  NOTE(koekeishiya): A list of all built-in modifier and literal keywords can
#                     be found at https://github.com/koekeishiya/skhd/issues/1
#
#                     A hotkey is written according to the following rules:
#
#                       hotkey   = <keysym> ':' <command> |
#                                  <keysym> '->' ':' <command>
#
#                       keysym   = <mod> '-' <key> | <key>
#
#                       mod      = 'built-in mod keyword' | <mod> '+' <mod>
#
#                       key      = <literal> | <keycode>
#
#                       literal  = 'single letter or built-in keyword'
#
#                       keycode  = 'apple keyboard kVK_<Key> values (0x3C)'
#
#                       ->       = keypress is not consumed by skhd
#
#                       command  = command is executed through '$SHELL -c' and
#                                  follows valid shell syntax. if the $SHELL environment
#                                  variable is not set, it will default to '/bin/bash'.
#                                  when bash is used, the ';' delimeter can be specified
#                                  to chain commands.
#
#                                  to allow a command to extend into multiple lines,
#                                  prepend '\' at the end of the previous line.
#
#                                  an EOL character signifies the end of the bind.

# Load secondary skhd configs, e.g. for work
.load ".work_skhdrc"

# open terminal, blazingly fast compared to iTerm/Hyper
cmd + ctrl + alt - return : open -na /Applications/Alacritty.app

# open new chrome window
cmd + ctrl + alt - space : open -na "Google Chrome" --args --new-window

# lock screen, start screensaver
#cmd + ctrl + alt - l : /System/Library/CoreServices/ScreenSaverEngine.app/Contents/MacOS/ScreenSaverEngine
cmd + ctrl + alt - l : /usr/bin/pmset displaysleepnow

# close focused window
#alt - w : chunkc tiling::window --close

# focus window
cmd + ctrl + alt - left : yabai -m window --focus west
cmd + ctrl + alt - down : yabai -m window --focus south
cmd + ctrl + alt - up : yabai -m window --focus north
cmd + ctrl + alt - right : yabai -m window --focus east

#cmd - j : chunkc tiling::window --focus prev
#cmd - k : chunkc tiling::window --focus next

# equalize size of windows
shift + alt - 0 : yabai -m space --balance

# swap window
#shift + alt - h : chunkc tiling::window --swap west
#shift + alt - j : chunkc tiling::window --swap south
#shift + alt - k : chunkc tiling::window --swap north
#shift + alt - l : chunkc tiling::window --swap east

# move window
shift + cmd + ctrl + alt - left : yabai -m window --warp west
shift + cmd + ctrl + alt - down : yabai -m window --warp south
shift + cmd + ctrl + alt - up : yabai -m window --warp north
shift + cmd + ctrl + alt - right : yabai -m window --warp east

# move floating windows / windows on a floating space
#shift + alt - up     : chunkc tiling::window --warp-floating fullscreen
#shift + alt - left   : chunkc tiling::window --warp-floating left
#shift + alt - right  : chunkc tiling::window --warp-floating right
#shift + cmd - left   : chunkc tiling::window --warp-floating top-left
#shift + cmd - right  : chunkc tiling::window --warp-floating top-right
#shift + ctrl - left  : chunkc tiling::window --warp-floating bottom-left
#shift + ctrl - right : chunkc tiling::window --warp-floating bottom-right

# send window to desktop
#shift + alt - x : chunkc tiling::window --send-to-desktop $(chunkc get _last_active_desktop)
#shift + alt - z : chunkc tiling::window --send-to-desktop prev
#shift + alt - c : chunkc tiling::window --send-to-desktop next
#shift + alt - 1 : chunkc tiling::window --send-to-desktop 1
#shift + alt - 2 : chunkc tiling::window --send-to-desktop 2
#shift + alt - 3 : chunkc tiling::window --send-to-desktop 3
#shift + alt - 4 : chunkc tiling::window --send-to-desktop 4
#shift + alt - 5 : chunkc tiling::window --send-to-desktop 5
#shift + alt - 6 : chunkc tiling::window --send-to-desktop 6

# focus monitor
#ctrl + alt - z  : chunkc tiling::monitor -f prev
#ctrl + alt - c  : chunkc tiling::monitor -f next
#ctrl + alt - 1  : chunkc tiling::monitor -f 1
#ctrl + alt - 2  : chunkc tiling::monitor -f 2
#ctrl + alt - 3  : chunkc tiling::monitor -f 3

# send window to monitor and follow focus
#ctrl + cmd - z  : chunkc tiling::window --send-to-monitor prev; chunkc tiling::monitor -f prev
#ctrl + cmd - c  : chunkc tiling::window --send-to-monitor next; chunkc tiling::monitor -f next
#ctrl + cmd - 1  : chunkc tiling::window --send-to-monitor 1; chunkc tiling::monitor -f 1
#ctrl + cmd - 2  : chunkc tiling::window --send-to-monitor 2; chunkc tiling::monitor -f 2
#ctrl + cmd - 3  : chunkc tiling::window --send-to-monitor 3; chunkc tiling::monitor -f 3

# Toggle windows between displays
shift + ctrl + cmd + alt - tab : yabai -m window --display prev
ctrl + cmd + alt - tab : yabai -m window --display next

# increase region size
#shift + alt - a : chunkc tiling::window --use-temporary-ratio 0.1 --adjust-window-edge west
#shift + alt - s : chunkc tiling::window --use-temporary-ratio 0.1 --adjust-window-edge south
#shift + alt - w : chunkc tiling::window --use-temporary-ratio 0.1 --adjust-window-edge north
#shift + alt - d : chunkc tiling::window --use-temporary-ratio 0.1 --adjust-window-edge east

# decrease region size
#shift + cmd - a : chunkc tiling::window --use-temporary-ratio -0.1 --adjust-window-edge west
#shift + cmd - s : chunkc tiling::window --use-temporary-ratio -0.1 --adjust-window-edge south
#shift + cmd - w : chunkc tiling::window --use-temporary-ratio -0.1 --adjust-window-edge north
#shift + cmd - d : chunkc tiling::window --use-temporary-ratio -0.1 --adjust-window-edge east

# set insertion point for focused container
#ctrl + alt - f : chunkc tiling::window --use-insertion-point cancel
#ctrl + alt - h : chunkc tiling::window --use-insertion-point west
#ctrl + alt - j : chunkc tiling::window --use-insertion-point south
#ctrl + alt - k : chunkc tiling::window --use-insertion-point north
#ctrl + alt - l : chunkc tiling::window --use-insertion-point east

# rotate tree
cmd + ctrl + alt - r : yabai -m space --rotate 90

# mirror tree y-axis
alt - y : yabai -m space --mirror y-axis

# mirror tree x-axis
alt - x : yabai -m space --mirror x-axis

# toggle desktop offset
cmd + ctrl + alt - a : yabai -m space --toggle padding; yabai -m space --toggle gap

# toggle window fullscreen
cmd + ctrl + alt - f : yabai -m window --toggle zoom-fullscreen

# toggle window native fullscreen
#shift + alt - f : chunkc tiling::window --toggle native-fullscreen

# toggle window parent zoom
#alt - d : chunkc tiling::window --toggle parent

# toggle window split type
cmd + ctrl + alt - e : yabai -m window --toggle split

# float / unfloat window
cmd + ctrl + alt - t : yabai -m window --toggle float

# toggle sticky, float and resize to picture-in-picture size
#alt - s : chunkc tiling::window --toggle sticky; chunkc tiling::window --warp-floating pip-right

# float next window to be tiled
#shift + alt - t : chunkc set window_float_next 1

# change layout of desktop
cmd + ctrl + alt - a : yabai -m space --layout bsp
#ctrl + alt - s : chunkc tiling::desktop --layout monocle
#ctrl + alt - d : chunkc tiling::desktop --layout float

#ctrl + alt - w : chunkc tiling::desktop --deserialize ~/.chunkwm_layouts/dev_1
