HISTFILE=~/.histfile
HISTSIZE=1000000
SAVEHIST=1000000
# append to history immediately
setopt INC_APPEND_HISTORY
# include time and date of commands in history
setopt EXTENDED_HISTORY
bindkey -e

# The following lines were added by compinstall
zstyle :compinstall filename '~/.zshrc'

autoload -Uz compinit
compinit
# End of lines added by compinstall

COMPLETION_WAITING_DOTS="false"

# DISABLE AUTOCORRECTION
DISABLE_CORRECTION="true"

export EDITOR=nano

# Set tmux to use /var/tmp to keep tmux servers separate for e.g. distrobox
export TMUX_TMPDIR="/var/tmp"
