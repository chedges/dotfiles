if command -v atuin > /dev/null; then
    export ATUIN_NOBIND="true"
    eval "$(atuin init zsh --disable-ctrl-r)"
    bindkey '^[r' _atuin_search_widget
fi
