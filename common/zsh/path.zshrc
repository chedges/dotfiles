# add go env path to $PATH if go is installed
command -v go >/dev/null 2>&1 && export PATH="$(go env GOPATH)/bin:$PATH"

export PATH="$PATH:${HOME}/.local/bin"

# add ~/bin to $PATH for my own executables to reside in
export PATH=~/bin:$PATH
