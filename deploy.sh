#!/bin/bash

WORKDIR="$(cd "$(dirname "$0")" >/dev/null 2>&1 && pwd)"

mkdir -p "${HOME}/.config/alacritty"
mkdir -p "${HOME}/bin"

#Common
ln -snf "${WORKDIR}/common/starship.toml" "${HOME}/.config/starship.toml"
ln -snf "${WORKDIR}/common/zshrc" "${HOME}/.zshrc"
ln -snf "${WORKDIR}/common/nanorc" "${HOME}/.nanorc"
ln -snf "${WORKDIR}/common/nano" "${HOME}/.nano"
ln -snf "${WORKDIR}/common/alacritty.toml" "${HOME}/.config/alacritty/alacritty.toml"

if ! uname -s | grep -q 'Darwin'; then
    #Linux
    ln -snf "${WORKDIR}/linux/linuxzshrc" "${HOME}/.linuxzshrc"
    ln -snf "${WORKDIR}/linux/sway.config" "${HOME}/.config/sway/config"
    ln -snf "${WORKDIR}/linux/waybar/config.jsonc" "${HOME}/.config/waybar/config.jsonc"
    ln -snf "${WORKDIR}/linux/waybar/style.css" "${HOME}/.config/waybar/style.css"
    ln -snf "${WORKDIR}/linux/alacritty_linux.toml" "${HOME}/.config/alacritty/alacritty_linux.toml"
else
    mkdir -p "${HOME}/.config/"{karabiner,sketchybar}
    ln -snf "${WORKDIR}/mac/maczshrc" "${HOME}/.maczshrc"
    ln -snf "${WORKDIR}/mac/zshenv" "${HOME}/.zshenv"
    ln -snf "${WORKDIR}/mac/karabiner.json" "${HOME}/.config/karabiner/karabiner.json"
    ln -snf "${WORKDIR}/mac/skhdrc" "${HOME}/.skhdrc"
    ln -snf "${WORKDIR}/mac/yabairc" "${HOME}/.yabairc"
    ln -snf "${WORKDIR}/mac/Default.bttpreset" "${HOME}/.btt_autoload_preset.json"
#    ln -snf "${WORKDIR}/mac/alacritty_macos.toml" "${HOME}/.config/alacritty/alacritty_macos.toml"
    ln -snf "${WORKDIR}/mac/sketchybar_plugins" "${HOME}/.config/sketchybar/plugins"
    ln -snf "${WORKDIR}/mac/sketchybarrc" "${HOME}/.sketchybarrc"
    ln -snf "${WORKDIR}/mac/bin/"* "${HOME}/bin/"
fi
